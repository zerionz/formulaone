﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaOne
{
    public class RaceResult
    {
        public int TeamNumber { get; set; }

        public double FinalSpeed { get; set; }

        public double CompleteTime { get; set; }
    }
}
