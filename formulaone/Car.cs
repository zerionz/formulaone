﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FormulaOne
{
    public class Car
    {
        public Car(int teamNumber, double trackLength)
        {
            this.TeamNumber = teamNumber;
            this.TrackLength = trackLength;
            this.CurrentDistance = this.StartPoint;
            this.CurrentSpeed = 0;
            this.Time = 0;
            this.IsTopSpeed = false;
            this.IsFinished = false;
            this.IsUseNitro = false;
        }

        public double TrackLength { get; set; }

        public bool IsFinished { get; set; }

        public int TeamNumber { get; set; }

        public bool IsTopSpeed { get; set; }

        public double Time { get; set; }

        public bool IsUseNitro { get; set; }

        public double TopSpeed
        {
            get
            {
                return (150 + 10 * this.TeamNumber) * 0.278;
            }

            set
            {
                this.TopSpeed = value;
            }
        }

        public int Acceleration
        {
            get
            {
                return 2 * this.TeamNumber;
            }

            set
            {
                this.Acceleration = value;
            }
        }

        public double HandlingFactor
        {
            get
            {
                return 0.8;
            }

            set
            {
                this.HandlingFactor = value;
            }
        }

        public bool IsUseHandlingFactor { get; set; }

        public int StartPoint
        {
            get
            {
                return 200 * (this.TeamNumber - 1) * -1;
            }

            set
            {
                this.StartPoint = value;
            }
        }

        public void UseNitro()
        {
            if (this.IsUseNitro == false)
            {
                var result = this.CurrentSpeed * 2;
                if (result >= this.TopSpeed)
                {
                    this.CurrentSpeed = this.TopSpeed;
                }
                else
                {
                    this.CurrentSpeed = result;
                }
                this.IsUseNitro = true;
            }
        }

        public void UseHandlingFactor()
        {
            this.IsUseHandlingFactor = true;
        }

        public void UnUseHandlingFactor()
        {
            this.IsUseHandlingFactor = false;
        }

        public double CurrentDistance { get; set; }

        public double CurrentSpeed { get; set; }

        public double CalculateDistance()
        {
            if (this.IsTopSpeed)
            {
                return this.CurrentSpeed;
            }
            else
            {
                return this.CurrentSpeed + this.Acceleration / 2;
            }
        }

        public double CalculateVelocity()
        {
            if (this.IsTopSpeed)
            {
                return this.IsUseHandlingFactor ? this.TopSpeed * this.HandlingFactor : this.TopSpeed;
            }
            else
            {
                var expectSpeed = this.CurrentSpeed + this.Acceleration;
                if (expectSpeed > this.TopSpeed)
                {
                    return this.IsUseHandlingFactor ? this.TopSpeed * this.HandlingFactor : this.TopSpeed;
                }
                else
                {
                    return this.IsUseHandlingFactor ? expectSpeed * this.HandlingFactor : expectSpeed;
                }
            }
        }

        public double CaculateLastTimeWhenTopSpeed()
        {
            var s = this.TrackLength - this.CurrentDistance;
            var v = this.CurrentSpeed;
            return s / v;
        }

        public double CaculateLastTimeWhenAccerate()
        {
            var s = this.TrackLength - this.CurrentDistance;
            var u = this.CurrentSpeed;
            var a = this.Acceleration;
            return 2 * s / (u + a);
        }

        public Task Accelerate()
        {
            if (!IsFinished)
            {
                return Task.Run(() =>
                {
                    if (this.CurrentDistance + CalculateDistance() >= this.TrackLength)
                    {
                        if (this.IsTopSpeed)
                        {
                            this.Time += CaculateLastTimeWhenTopSpeed();
                        }
                        else
                        {
                            this.Time += CaculateLastTimeWhenAccerate();
                        }
                        this.IsFinished = true;
                    }
                    else
                    {
                        this.CurrentDistance += CalculateDistance();
                        this.Time++;

                        this.CurrentSpeed = CalculateVelocity();
                        if (this.CurrentSpeed >= this.TopSpeed)
                        {
                            this.IsTopSpeed = true;
                        }
                    }
                    return this.CurrentDistance;
                });
            }
            else
            {
                return null;
            }
        }
    }
}
