﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Race r = new Race(5000, 5000);

            var result = r.BeginRace();
            r.PrettyPrint();
        }
    }
}
