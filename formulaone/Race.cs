﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FormulaOne
{
    public class Race
    {
        public Race(int numberOfTeam, double trackLength)
        {
            this.NumberOfTeam = numberOfTeam;
            this.TrackLength = trackLength;
            this.FinishedCarList = new List<Car>();
            this.TeamList = new List<Car>();
            for (var i = 1; i <= this.NumberOfTeam; i++)
            {
                this.TeamList.Add(new Car(i, this.TrackLength));
            }
            Console.WriteLine("Start Race!!");
        }

        public double TrackLength { get; protected set; }

        public int NumberOfTeam { get; protected set; }

        public List<Car> TeamList { get; set; }

        public List<Car> FinishedCarList { get; set; }

        public List<RaceResult> BeginRace()
        {
            bool needCheckAssessment = false;
            while (true)
            {
                var avaliableCars = new List<Task>();
                foreach (var car in this.TeamList)
                {
                    var task = car.Accelerate();
                    if (task != null)
                    {
                        avaliableCars.Add(task);
                    }
                }

                Task.WaitAll(avaliableCars.ToArray());

                if (needCheckAssessment)
                {
                    if (IsFinish())
                    {
                        break;
                    }
                    else
                    {
                        CheckAssesment();
                    }
                }
                needCheckAssessment = !needCheckAssessment;
            }
            return DecoratedResult();
        }

        public void CheckAssesment()
        {
            this.TeamList = this.TeamList.OrderByDescending(d => d.CurrentDistance).ToList();
            CheckUsingNitro();
            CheckCloserCars();
        }

        public void CheckUsingNitro()
        {
            var last = this.TeamList.LastOrDefault();
            if (last != null)
            {
                last.UseNitro();
            }
        }

        public void CheckCloserCars()
        {
            if (this.TeamList.Count > 1)
            {
                for (var i = 0; i < this.TeamList.Count; i++)
                {
                    var currentDriver = this.TeamList.ElementAt(i);
                    var nextDriver = this.TeamList.ElementAtOrDefault(i + 1);
                    var prevDriver = this.TeamList.ElementAtOrDefault(i - 1);
                    if ((nextDriver != null && Math.Abs(currentDriver.CurrentDistance - nextDriver.CurrentDistance) <= 10) || (prevDriver != null && Math.Abs(currentDriver.CurrentDistance - prevDriver.CurrentDistance) <= 10))
                    {
                        currentDriver.UseHandlingFactor();
                    }
                    else
                    {
                        currentDriver.UnUseHandlingFactor();
                    }
                }
            }
        }

        public bool IsFinish()
        {
            var finishedCars = this.TeamList.Where(d => d.IsFinished).ToList();
            if (finishedCars.Count > 0)
            {
                foreach (var car in finishedCars)
                {
                    //Console.WriteLine("{0} is Finish with SPEED {1} and Elasped is {2}", car.TeamNumber, car.CurrentSpeed, car.Time);
                    this.TeamList.Remove(car);
                    this.FinishedCarList.Add(car);
                }
            }
            if (this.TeamList.Count == 0)
            {
                return true;
            }
            return false;
        }

        public List<RaceResult> DecoratedResult()
        {
            return this.FinishedCarList.OrderBy(d => d.Time).Select(d => new RaceResult
            {
                TeamNumber = d.TeamNumber,
                FinalSpeed = d.CurrentSpeed,
                CompleteTime = d.Time,
            }).ToList();
        }

        public void PrettyPrint()
        {
            var result = DecoratedResult();
            for (var i = 0; i < result.Count; i++)
            {
                var res = result.ElementAt(i);
                Console.WriteLine("Place: {0}, TeamNumber: {1}, FinalSpeed: {2} m/s, CompleteTime: {3} sec", i + 1, res.TeamNumber, res.FinalSpeed, res.CompleteTime);
            }
        }
    }
}
